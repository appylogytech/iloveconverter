class UFile {
    uFile;
    constructor(file) {
        this.uFile = file
    }

}
class UFiles {
    constructor() {
        this.uFiles = []
    }

    newFiles(file) {
        let ufile = new UFile(file)
        this.uFiles.push(ufile)
        return ufile
    }

    get allUFiles() {
        return this.uFiles
    }

    get numberOfFiles() {
        return this.uFiles.length
    }
}
class Img {
    id;
    img
    width;
    height;
    type;
    size;
    constructor(id, img, width, height, type, size) {
        this.img = img
        this.id = id
        this.width = width
        this.height = height
        this.type = type
        this.size = size
    }
}
class Imgs {
    constructor() {
        this.imgs = []
    }
    addImg(id, img, width, height, type, size) {
        let i = new Img(id, img, width, height, type, size)
        this.imgs.push(i)
        return i
    }
}

let uFile = new UFiles()
let uImg = new Imgs()

var selectCount = 0


var imageMimes = ['image/png', 'image/jpeg', 'image/webp',];
let uploadFiles = document.getElementById('uploadFiles');

var body = document.getElementById("prw")
uploadFiles.addEventListener('change', addFiles);
isFirst = true
counter = 0


var form = document.getElementById("selectType");






function addFiles(event) {
    totalUploadedFiles = event.target.files.length;

    if (event.target.files) {
        if (isFirst) {
            for (let i = 0; i < totalUploadedFiles; i++) {
                uFile.newFiles(event.target.files[i])
                drawOnCanvas(uFile.uFiles[i].uFile, i)
            }
            counter = totalUploadedFiles
            isFirst = false
        } else {
            console.log("here");
            uFile.newFiles(event.target.files[0])
            drawOnCanvas(uFile.uFiles[counter++].uFile, 0)
        }

        console.log(event.target.files);
    }
    else {
        alert('please select valid files')
    }
}


function drawOnCanvas(file, i) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function (e) {
        var img = new Image(); // Creates image object
        img.src = e.target.result;
        img.onload = function (ev) {
            uImg.addImg(i, img, img.width, img.height, file.type, file.size)
            createPreview(img, 200, 200, i)
            // resultImg(i, img, file.type, file.size)

        }
    }
}

function createPreview(img, width, height, id) {
    var canvas = document.createElement('canvas');
    canvas.id = 'canvas_0' + id;
    canvas.width = width;
    canvas.height = height;
    canvas.style.border = "1px solid";

    canvas.style.margin = "10px";
    canvas.style.borderRadius = '10px';

    body.appendChild(canvas);
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);
}

form.addEventListener('change', function (event) {
    switch (event.target.value) {
        case "0":
            // r1.style.background = "#FFD900"

            r1.style.background = "#b92b27"
            r1.style.background = "-webkit-linear-gradient(to right, #1565C0, #b92b27)"
            r1.style.background = "linear-gradient(to right, #1565C0, #b92b27)"

            r2.style.background = "#ffefba"
            r2.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r2.style.background = "linear-gradient(to right, #ffefba, #ffffff)"

            r3.style.background = "#ffefba"
            r3.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r3.style.background = "linear-gradient(to right, #ffefba, #ffffff)"

            break;
        case "1":
            // r2.style.background = "#FFD900"
            r2.style.background = "#b92b27"
            r2.style.background = "-webkit-linear-gradient(to right, #1565C0, #b92b27)"
            r2.style.background = "linear-gradient(to right, #1565C0, #b92b27)"



            r1.style.background = "#ffefba"
            r1.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r1.style.background = "linear-gradient(to right, #ffefba, #ffffff)"

            r3.style.background = "#ffefba"
            r3.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r3.style.background = "linear-gradient(to right, #ffefba, #ffffff)"


            break;
        case "2":
            // r3.style.background = "#FFD900"

            r3.style.background = "#b92b27"
            r3.style.background = "-webkit-linear-gradient(to right, #1565C0, #b92b27)"
            r3.style.background = "linear-gradient(to right, #1565C0, #b92b27)"

            r2.style.background = "#ffefba"
            r2.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r2.style.background = "linear-gradient(to right, #ffefba, #ffffff)"

            r1.style.background = "#ffefba"
            r1.style.background = "-webkit-linear-gradient(to right, #ffefba, #ffffff)"
            r1.style.background = "linear-gradient(to right, #ffefba, #ffffff)"

            break;
    }
})

function resultImg(id, img, type, size) {
    return new Promise((resolve, reject) => {
        var original_canvas = document.createElement('canvas');
        original_canvas.width = img.width;
        original_canvas.height = img.height;
        original_canvas.id = 'original_canvas_' + id;
        var original_ctx = original_canvas.getContext("2d");
        original_ctx.drawImage(img, 0, 0);
        resolve(original_canvas)
    });


}

function changeFormate(canvas, type) {
    canvas.toBlob(function (blob) {
        var link = document.createElement('a');

        link.download = 'imageName_free_format'
        link.href = URL.createObjectURL(blob);
        link.click();
    }, imageMimes[type], 1);
}

async function downloadImg() {
    // jpegToPDF(0, uImg.imgs[0].img, 0, 200)

    let type = form.elements["type"].value;
    console.log(imageMimes[type]);
    for (let i = 0; i < uImg.imgs.length; i++) {
        var canvas = await resultImg(i, uImg.imgs[i].img, type, 200)
        changeFormate(canvas, type)
        setInterval(() => {

        }, 1000);
    }
}










